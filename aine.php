<?php
    function haeAineet() {
        
        $username = "tomipkaa";
        $password = "UhcKYqXaeAoJedMg";
        $database = "harjoitus_kanta";
        $host = "localhost";
        
        try {
            $conn = new PDO("mysql:host=$host;dbname=$database;charset=utf8", $username, $password);
            
            //print "Tietokannan avaus onnistui";
            
            $sql = "SELECT * FROM aine";
            
            $query = $conn->query($sql);
            $query->setFetchMode(PDO::FETCH_OBJ);
            
            while($row = $query->fetch()) {
                // printtaa HTML-dokumenttiin <tr> tagin
                print "<tr>";
				
				print "<td>";	
                    print $row->nimi;
                print "</td>";
                
                print "<td>";
                    print $row->laajuus;
                print "</td>";
            
                print "<td>" . $row->opettaja . "</td>";
                print "<td>" . $row->oppilas . "</td>";
                
                $nimi = $row->nimi;
                
                print "<td><a href='edit.php?nimi=$nimi'>Edit</a> <a href='delete.php?nimi=$nimi'>Delete</a></td>";
                
                // printtaa HTML-dokumenttii </tr> lopetustagin
                print "</tr>";
            }
            
            
        } catch(PDOException $pdoex) {
            print "Tietokannan avaus epäonnistui " . $pdoex->getMessage();
        }
        
        $conn = null;

    }
?>

<html>
    <head>
        <meta charset="utf-8">
        <title>Harjoitus_kanta aine</title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">

            <h4 class="mb-3">Aineet:</h4>
            
			<div class="table-responsive">
            <table class="table table-striped table-sm">
				<thead>
					<tr>
						<th>Nimi</th>
						<th>Laajuus</th>
						<th>Opettaja</th>
						<th>Oppilas</th>
						<th>Edit/Delete</th>
					</tr>
				</thead>
				<tbody>
                    <?php
                        haeAineet();
                    ?>
                    
				</tbody>
			</table>
				
			<button id="new">Lisää aine</button>
        
        </div>
        
        <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        
        <script>
            $(document).ready(function() {
                console.log("toimii");
                
                $("#new").click(function() {

                  window.location.href = "new.php";
                  
                });
            });
        </script>
    </body>
</html>