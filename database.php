<?php
$servername = "localhost";
$username = "tomipkaa";
$password = "UhcKYqXaeAoJedMg";
$dbname = "harjoitus_kanta";

try {
    $connection = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    // echo "Connected successfully";

    $stmt = $connection->prepare("SELECT * FROM aine");
    $stmt->execute();

    // vaihdetaan moodiksi objektit
    $stmt->setFetchMode(PDO::FETCH_OBJ);

    // haetaan kaikki rivit tietokannasta
    $result = $stmt->fetchAll();

    $nro = 1;

    // käydään läpi palautetut rivit yksi kerrallaan
    foreach($result as $row) {
        print $nro;
        print " ";
        print "<a href='metodi.php?nimi=$row->nimi&laajuus=$row->laajuus'>$row->nimi</a>";
        print "<br/>";

        $nro++;
    }
}
catch(PDOException $e)
{
echo "Connection failed: " . $e->getMessage();
}

// suljetaan tietokantayhteys
$connection = null;

?>