<?php

    // lue mikä aine (=nimi) pitää poistaa?

    $aineen_nimi = filter_input(INPUT_GET, 'nimi', FILTER_SANITIZE_STRING);
    
    $username = "tomipkaa";
    $password = "UhcKYqXaeAoJedMg";
    $database = "harjoitus_kanta";
    $host = "localhost";
    
    try {
        $conn = new PDO("mysql:host=$host;dbname=$database;charset=utf8", $username, $password);

        $sql = "DELETE FROM aine WHERE nimi = :nimi";
        
        $query = $conn->prepare($sql);
        $query->bindValue(":nimi", $aineen_nimi, PDO::PARAM_STR);
        
        $query->execute();
        
        print "<h3>Aine $aineen_nimi poistettu.</h3>";

        print "<a href='aine.php'>Takaisin</a>";
        
    } catch(PDOException $pdoex) {
        print "Tietokannan avaus epäonnistui " . $pdoex->getMessage();
    }
    
    $conn = null;
?>