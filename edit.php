<?php
    $username = "tomipkaa";
    $password = "UhcKYqXaeAoJedMg";
    $database = "harjoitus_kanta";
    $host = "localhost";
    
    $aineen_nimi = filter_input(INPUT_GET, 'nimi', FILTER_SANITIZE_STRING);
    $laajuus = filter_input(INPUT_POST, 'laajuus', FILTER_SANITIZE_NUMBER_INT);
    $opettaja = filter_input(INPUT_POST, 'opettaja', FILTER_SANITIZE_STRING);
    $oppilas = filter_input(INPUT_POST, 'oppilas', FILTER_SANITIZE_STRING);
    
    try {
        $conn = new PDO("mysql:host=$host;dbname=$database;charset=utf8", $username, $password);

        $sql = "SELECT * FROM aine WHERE nimi = '$aineen_nimi'";
        
        $query = $conn->query($sql);
        $query->setFetchMode(PDO::FETCH_OBJ);
        
        while($row = $query->fetch()) {
            $laajuus = $row->laajuus;
            $opettaja = $row->opettaja;
            $oppilas = $row->oppilas;
        }
        
    } catch(PDOException $pdoex) {
        print "Tietokannan avaus epäonnistui " . $pdoex->getMessage();
    }
    
    $conn = null;
?>

<html>
    <head>
        <meta charset="utf-8">
        <title>Harjoitus_kanta select esimerkki</title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">

            <h4 class="mb-3">Aineiden tiedot:</h4>
            <form id="ainetiedot" class="needs-validation" action="update.php" method="POST" validate>
                <div class="row">
                  <div class="col-md-5 mb-3">
                    <label for="nimi">Aineen nimi</label>
                    <input type="text" class="form-control" id="nimi" name="nimi" placeholder="" value="<?php print $aineen_nimi; ?>" >
                  </div>
                  <div class="col-md-5 mb-3">
                    <label for="laajuus">Laajuus</label>
                    <input type="text" class="form-control" id="laajuus" name="laajuus" placeholder="" value="<?php print $laajuus; ?>" >
                  </div>
                  <div class="col-md-2 mb-3">
                    <label for="ope">Opettaja</label>
                    <input type="text" class="form-control" id="ope" name="ope" placeholder="" value="<?php print $opettaja; ?>" >
                  </div>                  
                </div>
                <div class="row">
                  <div class="col-md-5 mb-3">
                    <label for="oppilas">Oppilas</label>
                    <input type="text" class="form-control" id="oppilas" name="oppilas" placeholder="" value="<?php print $oppilas; ?>" required>
                  </div>
                </div>                
                <input type="button" id="tallenna" value="Tallenna" />
            </form>
        
        </div>
        
        <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        
        <script>
            $(document).ready(function() {
                console.log("toimii");
                
                $("#nimi").prop("disabled", true);
                
                $("#tallenna").click(function() {
                  $("#ainetiedot").submit();
                });
            });
        </script>
    </body>
</html>