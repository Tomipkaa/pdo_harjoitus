<?php
    
    $nimi = filter_input(INPUT_GET, 'nimi', FILTER_SANITIZE_SPECIAL_CHARS);
    $laajuus = filter_input(INPUT_GET, 'laajuus', FILTER_SANITIZE_NUMBER_INT);

    print "<h1>$nimi</h1>";

    print "<h3>Oppiaineet</h3>";

    // asetellaan muuttujilla arvot
    $servername = "localhost";
    $username = "tomipkaa";
    $password = "UhcKYqXaeAoJedMg";
    $dbname = "harjoitus_kanta";

    try {
        $connection = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        // set the PDO error mode to exception
        $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // aloitetaan transaktion
        $connection->beginTransaction();

        // sql komennot
        $statement = $connection->prepare("SELECT * FROM metodi WHERE nimi='$nimi'");
        $statement->execute();

        // vaihdetaan hakumoodiksi objecti
        $statement->setFetchMode(PDO::FETCH_OBJ);

        // haetaan kaikki rivit
        $result = $statement->fetchAll();

        print "<table>";
        print "<tr>";
        print "<th>Nimi</th><th>Laajuus</th><th>Opettaja</th><th>Oppilas</th>";
        print "</tr>";

        foreach($result as $row) {
            print "<tr>";
            print "<td>$row->nimi</td><td>$row->laajuus</td><td>$row->opettaja</td><td>$row->oppilas</td>";      

            print "<td><a href='metodi.php?nimi=$row->nimi'>Avaa</a></td>";

            print "</tr>";
            //print "<br/>";      
        }

        print "</table>";

        // commit (hyväksytään transaktio)
        $connection->commit();
    }
    catch(PDOException $e)
    {
        // rollback eli perutaan transaktio
        $connection->rollback();

        echo "Tietokantavirhe: " . $e->getMessage();
    }

    // suljetaan tietokantayhteys
    $connection = null;
?>