<html>
    <head>
        <meta charset="utf-8">
        <title>Harjoitus_kanta select esimerkki</title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">

            <h4 class="mb-3">Aineiden tiedot:</h4>
            <form id="ainetiedot" class="needs-validation" action="save.php" method="POST" validate>
                <div class="row">
                  <div class="col-md-5 mb-3">
                    <label for="nimi">Aineen nimi</label>
                    <input type="text" class="form-control" id="nimi" name="nimi" placeholder="" value="" >
                  </div>
                  <div class="col-md-5 mb-3">
                    <label for="laajuus">Laajuus</label>
                    <input type="text" class="form-control" id="laajuus" name="laajuus" placeholder="" value="" >
                  </div>
                  <div class="col-md-2 mb-3">
                    <label for="opettaja">Opettaja</label>
                    <input type="text" class="form-control" id="opettaja" name="opettaja" placeholder="" value="" >
                  </div>                  
                </div>
                <div class="row">
                  <div class="col-md-5 mb-3">
                    <label for="oppilas">Oppilas</label>
                    <input type="text" class="form-control" id="oppilas" name="oppilas" placeholder="" value="" required>
                  </div>
                </div>                
                <input type="button" id="new" value="Lisää" />
            </form>
        
        </div>
        
        <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        
        <script>
            $(document).ready(function() {
                console.log("toimii");
                
                $("#new").click(function() {
                  $("#ainetiedot").submit();
                });
            });
        </script>
    </body>
</html>