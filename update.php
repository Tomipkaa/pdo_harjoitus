<html>
    <head>
        <meta charset="utf-8">
        <title>Harjoitus_kanta aine</title>
    </head>
    <body>

<?php

    $aineen_nimi = filter_input(INPUT_POST, 'nimi', FILTER_SANITIZE_STRING);
    $laajuus = filter_input(INPUT_POST, 'laajuus', FILTER_SANITIZE_NUMBER_INT);
    $opettaja = filter_input(INPUT_POST, 'opettaja', FILTER_SANITIZE_STRING);
    $oppilas = filter_input(INPUT_POST, 'oppilas', FILTER_SANITIZE_STRING);
    
    $username = "tomipkaa";
    $password = "UhcKYqXaeAoJedMg";
    $database = "harjoitus_kanta";
    $host = "localhost";
    
    try {
        $conn = new PDO("mysql:host=$host;dbname=$database;charset=utf8", $username, $password);

        $sql = "UPDATE aine SET laajuus = :laajuus, opettaja = :opettaja, oppilas = :oppilas
        WHERE nimi = :nimi";
        
        $query = $conn->prepare($sql);
        $query->bindValue(":nimi", $aineen_nimi, PDO::PARAM_STR);
        $query->bindValue(":laajuus", $laajuus, PDO::PARAM_INT);
        $query->bindValue(":opettaja", $opettaja, PDO::PARAM_STR);
        $query->bindValue(":oppilas", $oppilas, PDO::PARAM_STR);
        
        $query->execute();
        
        print "<h3>Tiedot päivitetty onnistuneesti</h3>";

        print "<a href='aine.php'>Takaisin</a>";
        
    } catch(PDOException $pdoex) {
        print "Tietokannan avaus epäonnistui " . $pdoex->getMessage();
    }
    
    $conn = null;

?>

    </body>
</html>